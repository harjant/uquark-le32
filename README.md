
# uquark-le32

A software implementation of the U-QUARK message digest.
This implementation is foremost meant for 32bit little-endian microcontrollers.


## Building

### For testing

There is a `main.c` file included which can be used for testing.
It will by default output the digest for an empty message.
You can build this by running `make`. Please see the `Makefile`.


### For inclusion in a project

Just include `uquark-le32.h` in your C language source code file
and call function `UQUARK_vUquarkDigest()` with the required parameters.
For more proper handling with split input buffer, like when reading from flash memory,
call the individual functions in sequence:

* UQUARK_vUquarkStateInit() - To initialize the state of the registers.

* UQUARK_vUquarkAbsorb() - Call several times with the required section of the message until everything is absorbed.

* UQUARK_vUquarkSqueeze() - To receive the digest for the whole message.

* UQUARK_vUquarkStateInit() - To initialize the state of the registers (clearing the state).

Please note that no effort on cleaning up of values left on stack is performed.


## Information

For information on the QUARK family of message digests, please read the paper titled
"QUARK: a lightweight hash" by Jean-Philippe Aumasson, Luca Henzen, Willi Meier and Mar�a Naya-Plasencia.

