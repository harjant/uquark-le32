/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

This file is part of uquark-le32, an implementation of the U-QUARK message digest.

Copyright (c) 2021-2022, Anton Harju
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* SPDX-License-Identifier: BSD-3-Clause */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "uquark-le32.h"


int main(void)
{
	uint8_t u8Idx;
	uint8_t au8Digest[17];
#if 0
	uint8_t au8Message[] = "Hello World!";

	UQUARK_vUquarkDigest( &au8Digest[0], &au8Message[0], strlen( (const char *) &au8Message[0] ) );
#else
	UQUARK_vUquarkDigest( &au8Digest[0], NULL, 0 );
#endif

	for ( u8Idx = 0; u8Idx < 17; u8Idx++ )
	{
		printf("%02x", au8Digest[u8Idx] );
	}
	printf("\n");
	return 0;
}
