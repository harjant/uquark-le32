/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

This file is part of uquark-le32, an implementation of the U-QUARK message digest.

Copyright (c) 2021-2022, Anton Harju
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* SPDX-License-Identifier: BSD-3-Clause */


#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "uquark-le32.h"

#define REGS_SHIFT_RIGHT 1


#define UQUARK_RATE 1
#define UQUARK_WIDTH 17

#if REGS_SHIFT_RIGHT < 1

/* This gives the same result for non-empty message as the reference implementation
but the empty message is then padded as 0x01 instead of 0x80. */
#define REF_IMPL_ABSORB_BIT_ORDER 1

/* These rightshift the 32bit value so the tap is at LSB, */
#define XTAP(a) ( psUquarkState->au32X[ ((a) / 32) ] >> ( (32 * ( ( (a) / 32 ) + 1) - 1 ) - (a) ) )
#define YTAP(a) ( psUquarkState->au32Y[ ((a) / 32) ] >> ( (32 * ( ( (a) / 32 ) + 1) - 1 ) - (a) ) )

typedef struct
{
	uint32_t au32X[2];
	uint8_t u8X;
	uint32_t au32Y[2];
	uint8_t u8Y;
} tsUquarkState;

const tsUquarkState c_sIvU =
{
	{ 0xd8daca44, 0x414a0997 }, 0x10,
	{ 0x9c80aa3a, 0xf065644d }, 0xb0
};

#else

#define XTAP(a) ( psUquarkState->au32X[ (1 - ((a) / 32 )) ] >> ( (a) - ( ( (a) / 32 ) * 32 ) ) )
#define YTAP(a) ( psUquarkState->au32Y[ (1 - ((a) / 32 )) ] >> ( (a) - ( ( (a) / 32 ) * 32 ) ) )

typedef struct
{
	uint8_t u8X;
	uint32_t au32X[2];
	uint8_t u8Y;
	uint32_t au32Y[2];
} tsUquarkState;

const tsUquarkState c_sIvU =
{
	0x08, { 0xe9905282, 0x22535b1b },
	0x0d, { 0xb226a60f, 0x5c550139 }
};
#endif

static void _vUquarkShowState( tsUquarkState *psUquarkState )
{
	printf("%08x",  psUquarkState->au32X[0] );
	printf("%08x",  psUquarkState->au32X[1] );
	printf("%1x", ( psUquarkState->u8X >> 4 ) );
	printf("%08x",  psUquarkState->au32Y[0] );
	printf("%08x",  psUquarkState->au32Y[1] );
	printf("%1x\n", ( psUquarkState->u8Y >> 4 ) );

	return;
}

static void _vUquarkPermute( tsUquarkState *psUquarkState )
{
	uint16_t u16Round;
	uint8_t u8XBit;
	uint8_t u8YBit;
	uint8_t u8LBit;
	uint8_t u8HBit;
	uint8_t u8Msb0;
	uint32_t u32Msb1;
	uint16_t u16L;
#if REGS_SHIFT_RIGHT > 0
	uint8_t u8Lsb0;
	uint32_t u32Lsb0;
#endif

	u16L = ~0;

	for ( u16Round = 0; u16Round < ( 4 * 8 * UQUARK_WIDTH ); u16Round++ )
	{
		/* * * * *
		*
		*  h^t
		*
		* * * * */

		u8HBit =
			( u16L >> ( 9 - 0 ) ) ^
			XTAP(1) ^
			YTAP(2) ^
			XTAP(4) ^
			YTAP(10) ^
			XTAP(25) ^
			XTAP(31) ^
			YTAP(43) ^
			XTAP(56) ^
			YTAP(59) ^
			(
				YTAP(3) &
				XTAP(55)
			) ^
			(
				XTAP(46) &
				XTAP(55)
			) ^
			(
				XTAP(55) &
				YTAP(59)
			) ^
			(
				YTAP(3) &
				XTAP(25) &
				XTAP(46)
			) ^
			(
				YTAP(3) &
				XTAP(46) &
				XTAP(55)
			) ^
			(
				YTAP(3) &
				XTAP(46) &
				YTAP(59)
			) ^
			(
				( u16L >> ( 9 - 0 ) ) &
				XTAP(25) &
				XTAP(46) &
				YTAP(59)
			) ^
			(
				( u16L >> ( 9 - 0 ) ) &
				XTAP(25)
			);

		u8HBit &= 1;

		/* * * * *
		*
		*  X
		*
		* * * * */

		/* f(X^t) */
		u8XBit = 
			XTAP(0) ^
			XTAP(9) ^
			XTAP(14) ^
			XTAP(21) ^
			XTAP(28) ^
			XTAP(33) ^
			XTAP(37) ^
			XTAP(45) ^
			XTAP(50) ^
			XTAP(52) ^
			XTAP(55) ^
			(
				XTAP(55) &
				XTAP(59)
			) ^
			(
				XTAP(33) &
				XTAP(37)
			) ^
			(
				XTAP(9) &
				XTAP(15)
			) ^
			(
				XTAP(45) &
				XTAP(52) &
				XTAP(55)
			) ^
			(
				XTAP(21) &
				XTAP(28) &
				XTAP(33)
			) ^
			(
				XTAP(9) &
				XTAP(28) &
				XTAP(45) &
				XTAP(59)
			) ^
			(
				XTAP(33) &
				XTAP(37) &
				XTAP(52) &
				XTAP(55)
			) ^
			(
				XTAP(15) &
				XTAP(21) &
				XTAP(55) &
				XTAP(59)
			) ^
			(
				XTAP(37) &
				XTAP(45) &
				XTAP(52) &
				XTAP(55) &
				XTAP(59)
			) ^
			(
				XTAP(9) &
				XTAP(15) &
				XTAP(21) &
				XTAP(28) &
				XTAP(33)
			) ^
			(
				XTAP(21) &
				XTAP(28) &
				XTAP(33) &
				XTAP(37) &
				XTAP(45) &
				XTAP(52)
			);

		/* + Y_0^t + h^t */
		u8XBit ^= 
			YTAP(0) ^
			u8HBit;

		u8XBit &= 1;

		/* * * * *
		*
		*  Y
		*
		* * * * */

		/* g(Y^t) */
		u8YBit = 
			YTAP(0) ^
			YTAP(7) ^
			YTAP(16) ^
			YTAP(20) ^
			YTAP(30) ^
			YTAP(35) ^
			YTAP(37) ^
			YTAP(42) ^
			YTAP(49) ^
			YTAP(51) ^
			YTAP(54) ^
			(
				YTAP(54) &
				YTAP(58)
			) ^
			(
				YTAP(35) &
				YTAP(37)
			) ^
			(
				YTAP(7) &
				YTAP(15)
			) ^
			(
				YTAP(42) &
				YTAP(51) &
				YTAP(54)
			) ^
			(
				YTAP(20) &
				YTAP(30) &
				YTAP(35)
			) ^
			(
				YTAP(7) &
				YTAP(30) &
				YTAP(42) &
				YTAP(58)
			) ^
			(
				YTAP(35) &
				YTAP(37) &
				YTAP(51) &
				YTAP(54)
			) ^
			(
				YTAP(15) &
				YTAP(20) &
				YTAP(54) &
				YTAP(58)
			) ^
			(
				YTAP(37) &
				YTAP(42) &
				YTAP(51) &
				YTAP(54) &
				YTAP(58)
			) ^
			(
				YTAP(7) &
				YTAP(15) &
				YTAP(20) &
				YTAP(30) &
				YTAP(35)
			) ^
			(
				YTAP(20) &
				YTAP(30) &
				YTAP(35) &
				YTAP(37) &
				YTAP(42) &
				YTAP(51)
			);
		/* + h^t */
		u8YBit ^= u8HBit;
		u8YBit &= 1;

		/* * * * *
		*
		*  L
		*
		* * * * */

		/* p(L^t) */
		u8LBit =
			( u16L >> ( 9 - 0 ) ) ^
			( u16L >> ( 9 - 3 ) );

		u8LBit &= 1;


		/* * * * *
		*
		*  Shift X
		*
		* * * * */
		
#if REGS_SHIFT_RIGHT < 1
		u8Msb0 = ( psUquarkState->u8X & 0x80 );
		psUquarkState->u8X <<= 1;
		psUquarkState->u8X |= ( u8XBit << 4 );

		u32Msb1 = ( psUquarkState->au32X[1] & 0x80000000 );
		psUquarkState->au32X[1] <<= 1;
		psUquarkState->au32X[1] |= (u8Msb0 >> 7);
		
		psUquarkState->au32X[0] <<= 1;
		psUquarkState->au32X[0] |= (u32Msb1 >> 31);
#else
		u8Lsb0 = ( psUquarkState->u8X & 0x01 ); 
		psUquarkState->u8X >>= 1;
		psUquarkState->u8X |= ( u8XBit << 3 );

		u32Lsb0 = ( psUquarkState->au32X[0] & 0x01 );
		psUquarkState->au32X[0] >>= 1;
		psUquarkState->au32X[0] |= ( ( (uint32_t) u8Lsb0 ) << 31 );

		psUquarkState->au32X[1] >>= 1;
		psUquarkState->au32X[1] |= ( u32Lsb0 << 31 );
#endif

		/* * * * *
		*
		*  Shift Y
		*
		* * * * */
		
#if REGS_SHIFT_RIGHT < 1
		u8Msb0 = ( psUquarkState->u8Y & 0x80 );
		psUquarkState->u8Y <<= 1;
		psUquarkState->u8Y |= ( u8YBit << 4 );

		u32Msb1 = ( psUquarkState->au32Y[1] & 0x80000000 );
		psUquarkState->au32Y[1] <<= 1;
		psUquarkState->au32Y[1] |= (u8Msb0 >> 7);
		
		psUquarkState->au32Y[0] <<= 1;
		psUquarkState->au32Y[0] |= (u32Msb1 >> 31);
#else

		u8Lsb0 = ( psUquarkState->u8Y & 0x01 ); 
		psUquarkState->u8Y >>= 1;
		psUquarkState->u8Y |= ( u8YBit << 3 );

		u32Lsb0 = ( psUquarkState->au32Y[0] & 0x01 );
		psUquarkState->au32Y[0] >>= 1;
		psUquarkState->au32Y[0] |= ( ( (uint32_t) u8Lsb0 ) << 31 );

		psUquarkState->au32Y[1] >>= 1;
		psUquarkState->au32Y[1] |= ( u32Lsb0 << 31 );


#endif
		/* * * * *
		*
		*  Shift L
		*
		* * * * */
		
		u16L <<= 1;
		u16L |= u8LBit;

	}

	return;
}

void UQUARK_vUquarkStateInit( tsUquarkState *psUquarkState)
{

	psUquarkState->au32X[0] = c_sIvU.au32X[0];
	psUquarkState->au32X[1] = c_sIvU.au32X[1];
	psUquarkState->u8X = c_sIvU.u8X;

	psUquarkState->au32Y[0] = c_sIvU.au32Y[0];
	psUquarkState->au32Y[1] = c_sIvU.au32Y[1];
	psUquarkState->u8Y = c_sIvU.u8Y;

	return;
}

void UQUARK_vUquarkAbsorb( tsUquarkState *psUquarkState, const uint8_t *pcu8MessageIn, uint32_t u32MessageLen )
{
#if REGS_SHIFT_RIGHT < 1
#if REF_IMPL_ABSORB_BIT_ORDER > 0
	uint8_t u8Byte;
	uint8_t u8Idx;
	uint8_t u8Shift;
	uint8_t u8Bit;
	uint8_t u8Xor;

	while ( u32MessageLen > 0 )
	{

		u8Byte = *pcu8MessageIn;
		u8Shift = 7;
		for ( u8Idx = 0; u8Idx < 8; u8Idx++ )
		{
			u8Bit = u8Byte & 1;

			u8Xor = u8Bit;
			u8Xor <<= u8Shift;

			psUquarkState->au32Y[1] ^= (uint32_t) (u8Xor >> 4);
			psUquarkState->u8Y ^= (u8Xor << 4);

			u8Shift--;
			u8Byte >>= 1;
		}

		pcu8MessageIn++;
		u32MessageLen--;

		_vUquarkPermute( psUquarkState );
	}
	return;
#else
	while ( u32MessageLen > 0 )
	{
		psUquarkState->au32Y[1] ^= (uint32_t) (*pcu8MessageIn >> 4);
		psUquarkState->u8Y ^= (*pcu8MessageIn << 4);

		pcu8MessageIn++;
		u32MessageLen--;

		_vUquarkPermute( psUquarkState );
	}
	return;
#endif
#else
	while ( u32MessageLen > 0 )
	{
		psUquarkState->au32Y[0] ^= (uint32_t) (*pcu8MessageIn << 28);
		psUquarkState->u8Y ^= (*pcu8MessageIn >> 4);

		pcu8MessageIn++;
		u32MessageLen--;

		_vUquarkPermute( psUquarkState );
	}
	return;
#endif
}

void UQUARK_vUquarkSqueeze( tsUquarkState *psUquarkState, uint8_t *pu8DigestOut )
{
	uint8_t u8Idx;
	uint8_t u8Nibble;
	uint8_t u8Byte;


	/* Padding */
#if REGS_SHIFT_RIGHT < 1
	psUquarkState->au32Y[1] ^= 0x08;
#else
	psUquarkState->au32Y[0] ^= (1 << 28);
#endif
	_vUquarkPermute( psUquarkState );



	memset(pu8DigestOut, 0, UQUARK_WIDTH );
	
	for ( u8Idx = 0; u8Idx < ( UQUARK_WIDTH - 1 ); u8Idx++ )
	{
#if REGS_SHIFT_RIGHT < 1
		pu8DigestOut[u8Idx] = ( ( (uint8_t) ( psUquarkState->au32Y[1] & 0x0F ) ) << 4 ) | ( psUquarkState->u8Y >> 4 );
#else
		u8Nibble = (uint8_t) ( psUquarkState->au32Y[0] >> 28 );
		u8Nibble |= ( ( u8Nibble & 0x08 ) << 1 );
		u8Nibble |= ( ( u8Nibble & 0x04 ) << 3 );
		u8Nibble |= ( ( u8Nibble & 0x02 ) << 5 );
		u8Nibble |= ( ( u8Nibble & 0x01 ) << 7 );
		
		pu8DigestOut[u8Idx] = ( u8Nibble & 0xF0 );

		u8Nibble = ( psUquarkState->u8Y & 0x0F );
		u8Nibble |= ( ( u8Nibble & 0x08 ) << 1 );
		u8Nibble |= ( ( u8Nibble & 0x04 ) << 3 );
		u8Nibble |= ( ( u8Nibble & 0x02 ) << 5 );
		u8Nibble |= ( ( u8Nibble & 0x01 ) << 7 );
		u8Nibble >>= 4;

		pu8DigestOut[u8Idx] |= u8Nibble;
#endif
		_vUquarkPermute( psUquarkState );
	}
#if REGS_SHIFT_RIGHT < 1
	pu8DigestOut[u8Idx] = ( ( (uint8_t) ( psUquarkState->au32Y[1] & 0x0F ) ) << 4 ) | ( psUquarkState->u8Y >> 4 );
#else
	u8Nibble = (uint8_t) ( psUquarkState->au32Y[0] >> 28 );
	u8Nibble |= ( ( u8Nibble & 0x08 ) << 1 );
	u8Nibble |= ( ( u8Nibble & 0x04 ) << 3 );
	u8Nibble |= ( ( u8Nibble & 0x02 ) << 5 );
	u8Nibble |= ( ( u8Nibble & 0x01 ) << 7 );
	
	pu8DigestOut[u8Idx] = ( u8Nibble & 0xF0 );

	u8Nibble = ( psUquarkState->u8Y & 0x0F );
	u8Nibble |= ( ( u8Nibble & 0x08 ) << 1 );
	u8Nibble |= ( ( u8Nibble & 0x04 ) << 3 );
	u8Nibble |= ( ( u8Nibble & 0x02 ) << 5 );
	u8Nibble |= ( ( u8Nibble & 0x01 ) << 7 );
	u8Nibble >>= 4;

	pu8DigestOut[u8Idx] |= u8Nibble;
#endif

	return;
}

void UQUARK_vUquarkDigest( uint8_t * pu8DigestOut, const uint8_t *pcu8MessageIn, uint32_t u32MessageLen )
{
	tsUquarkState sUquarkState;

	UQUARK_vUquarkStateInit( &sUquarkState );
	UQUARK_vUquarkAbsorb( &sUquarkState, pcu8MessageIn, u32MessageLen );
	UQUARK_vUquarkSqueeze( &sUquarkState, pu8DigestOut );
	UQUARK_vUquarkStateInit( &sUquarkState );

	return;
}

