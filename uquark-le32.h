/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

This file is part of uquark-le32, an implementation of the U-QUARK message digest.

Copyright (c) 2021, Anton Harju
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/* SPDX-License-Identifier: BSD-3-Clause */


#include <stdlib.h>
#include <stdint.h>

void UQUARK_vUquarkDigest( uint8_t *pu8DigestOut, const uint8_t *pcu8MessageIn, uint32_t u32MessageLen );

